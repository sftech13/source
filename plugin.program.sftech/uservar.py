import xbmcaddon
addon_id = xbmcaddon.Addon().getAddonInfo('id')

'''#####-----Build File-----#####'''
buildfile = 'https://bitbucket.org/sftech13/text_files/raw/master/builds.json'

'''#####-----Notifications File-----#####'''
notify_url  = 'https://bitbucket.org/sftech13/text_files/raw/master/notify.txt'

'''#####-----Excludes-----#####'''
excludes  = [addon_id, 'packages', 'Addons33.db', 'kodi.log', 'script.module.certifi', 'script.module.chardet', 'script.module.idna', 'script.module.requests', 'script.module.urllib3', 'backups']